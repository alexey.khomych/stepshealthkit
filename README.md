# StepsHealthKit



## About the project

Demonstrate architecture pattern MVVM with coordinator. Working with reusable tableView cells, views and network layer.  
In this project I worked with HeathKit and implement steps counter app. The app is requesting permission to HealthKit, then take data from backend which contains max steps count for day. 

Апка сделана на паттерне мввм. реализован переиспользуемый сетевой слой. 
апка запрашивает доступ к хелскиту. получает шаги с устройства, фильтрует результат (защита от накрутки и ввода руками), отправляет запрос на бекэнд и в ответе получает максимальное количество шагов в сутки. затем сверяет данные и если пользователь выполнил условия - отправляет запрос с данными на бэк, показывает поздравительное сообщение и в сохраняет дату в UserDefaults, чтобы не дублировать работу.   
